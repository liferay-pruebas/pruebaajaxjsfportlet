package org.ajax.web.managedBeans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

@ManagedBean(name = "prueba")
@ViewScoped
public class PruebaManagedBean implements Serializable {
	
	private String text;
	private String text2;
	
	
	public PruebaManagedBean() {
		
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText2() {
		return text2;
	}
	
	public void setText2(String text2) {
		this.text2 = text2;
	}
	
	public void ajaxListener(AjaxBehaviorEvent abe){
		System.out.println("Llamada ajax");
	}
}
